package xotv.android;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xotv.android.Adapters.RatesAdapter;
import xotv.android.Interfaces.RecyclerItemClickListener;
import xotv.android.Models.CountryRateModel;
import xotv.android.Utils.NumberUtils;
import xotv.android.network.Responses.CurrencyModel;
import xotv.android.network.RetrofitApi;
import xotv.android.network.RevolutAPI;

public class MainActivity extends AppCompatActivity implements RecyclerItemClickListener {
	private List<CountryRateModel> countryRateModels = new ArrayList<>();
	private RatesAdapter ratesAdapter;
	TextView stickedCountryName;
	EditText stickedCountryRate;
	Double stickedRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		stickedCountryName = findViewById(R.id.name);
		stickedCountryRate = findViewById(R.id.rate);
		RecyclerView recyclerView = findViewById(R.id.ratesList);

		recyclerView.setItemAnimator(null);
		ratesAdapter = new RatesAdapter(countryRateModels, this);
		recyclerView.setAdapter(ratesAdapter);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
		recyclerView.setLayoutManager(mLayoutManager);
		handler.post(periodicUpdate);
		bindStickedData("EUR", 1D);

	}

	private void bindStickedData(String countryName, Double rate) {
		stickedCountryRate.removeTextChangedListener(textWatcher);
		stickedCountryName.setText(countryName);
		stickedCountryRate.setText(String.valueOf(rate));
		stickedRate = rate;
		stickedCountryRate.setEnabled(true);
		stickedCountryRate.addTextChangedListener(textWatcher);


	}

	Handler handler = new Handler();
	private Runnable periodicUpdate = new Runnable() {
		public void run() {
			RetrofitApi
					.getInstance()
					.create(RevolutAPI.class)
					.getCurrencies()
					.enqueue(new Callback<CurrencyModel>() {
						@Override
						public void onResponse(Call<CurrencyModel> call, Response<CurrencyModel> response) {
							for (Map.Entry<String, Double> item : response.body().getRates().entrySet()) {
								CountryRateModel countryRateModel = new CountryRateModel();
								countryRateModel.setCountryCode(item.getKey());
								countryRateModel.setRate(item.getValue());
								ratesAdapter.updateItem(countryRateModel);
							}
						}

						@Override
						public void onFailure(Call<CurrencyModel> call, Throwable t) {

						}
					});
			handler.postDelayed(periodicUpdate, 1000);
		}
	};

	@Override
	public void onItemClick(CountryRateModel item, int position) {
		bindStickedData(item.getCountryCode(), item.getRate());
	}

	TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start,
		                              int count, int after)
		{
		}

		@Override
		public void onTextChanged(final CharSequence s, int start,
		                          int before, int count)
		{

			if (!s.toString().isEmpty()) {
				ratesAdapter.setRateChanges(NumberUtils.round(Double.valueOf(s.toString()) / stickedRate, 2));
			} else {
				ratesAdapter.setRateChanges(0);
			}
		}
	};

}
