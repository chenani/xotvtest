package xotv.android.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import xotv.android.Interfaces.RecyclerItemClickListener;
import xotv.android.Models.CountryRateModel;
import xotv.android.R;
import xotv.android.Utils.NumberUtils;

public class RatesAdapter extends RecyclerView.Adapter<RatesAdapter.RatesViewHolder> {
	private List<CountryRateModel> items;
	private double rateChanges = 1;
	private int stickedItemPosition = -1;
	private final RecyclerItemClickListener listener;
	private HashMap<String, Integer> dataPositions;

	public RatesAdapter(List<CountryRateModel> rates, RecyclerItemClickListener listener)
	{
		this.items = rates;
		this.listener = listener;
		dataPositions = new HashMap<>();
	}

	public void setRateChanges(double changes) {
		this.rateChanges = changes;
		if (items.size() > 0) {
			notifyDataSetChanged();
		}
	}

	public void addItem(CountryRateModel item) {
		items.add(item);
		dataPositions.put(item.getCountryCode(), items.size() - 1);
		notifyItemInserted(items.size() - 1);
	}
	public void updateItem(CountryRateModel item) {
		if (dataPositions.containsKey(item.getCountryCode())) {
			replaceItem(dataPositions.get(item.getCountryCode()), item);
		} else {
			addItem(item);
		}
	}

	public void stickItem(int position){
		rateChanges = 1;
		notifyDataSetChanged();
		if(stickedItemPosition !=-1){
			items.get(stickedItemPosition).setSticked(false);
			notifyItemChanged(stickedItemPosition);
		}
		items.get(position).setSticked(true);
		notifyItemChanged(position);
		stickedItemPosition = position;
	}

	public void replaceItem(int itemPosition, CountryRateModel item) {
		items.set(itemPosition, item);
		notifyItemChanged(itemPosition);
	}

	@NonNull
	@Override
	public RatesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		return new RatesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_item, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull RatesViewHolder ratesViewHolder, int i) {
		ratesViewHolder.bind(items.get(i),i);
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	class RatesViewHolder extends RecyclerView.ViewHolder {

		public TextView name;
		public EditText rate;
		public RatesViewHolder(@NonNull View itemView) {
			super(itemView);
			name = itemView.findViewById(R.id.name);
			rate = itemView.findViewById(R.id.rate);
		}

		public void bind(final CountryRateModel item, final int position){
			if(item.isSticked())
			{
				hideCard();
				return;
			}else {
				itemView.setVisibility(View.VISIBLE);
			}
			name.setText(item.getCountryCode());
			rate.setText(String.valueOf(NumberUtils.round(item.getRate()*rateChanges,4)));

			itemView.setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View v) {
					stickItem(position);
					listener.onItemClick(item,position);
				}
			});
		}

		private void hideCard(){
			itemView.setVisibility(View.GONE);
			itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
		}
	}
}
