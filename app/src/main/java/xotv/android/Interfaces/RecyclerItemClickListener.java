package xotv.android.Interfaces;

import xotv.android.Models.CountryRateModel;

public interface RecyclerItemClickListener {
	void onItemClick(CountryRateModel item, int position);
}
