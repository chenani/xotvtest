package xotv.android.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi {
    private static RetrofitApi instance;
    private OkHttpClient.Builder okHttpBuilder;
    private Retrofit retrofit;
    public static final int timeout = 120; //in seconds


    private RetrofitApi() {
    }

    public static RetrofitApi getInstance() {
        if (instance == null) {
            synchronized (RetrofitApi.class) {
                if (instance == null) {
                    instance = new RetrofitApi();
                }
            }
        }
        return instance;
    }

    public <T> T create(Class<T> clazz) {
        initOkHttp();
        initRetrofit();
        return retrofit.create(clazz);
    }

    private void initOkHttp() {
        if (okHttpBuilder != null && !okHttpBuilder.interceptors().contains("Authorization")) {
            okHttpBuilder = null; //to ensure headers are set and also prevent recreating unneeded objects
        }

        if (okHttpBuilder == null) {
            okHttpBuilder = new OkHttpClient().newBuilder();
            okHttpBuilder.readTimeout(timeout, TimeUnit.SECONDS);
            okHttpBuilder.connectTimeout(timeout, TimeUnit.SECONDS);
            okHttpBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request.Builder requestBuilder = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/json");
                    return chain.proceed(requestBuilder.build());
                }
            });
        }
    }

    private void initRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://revolut.duckdns.org")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpBuilder.build())
                    .build();
        }
    }
}
