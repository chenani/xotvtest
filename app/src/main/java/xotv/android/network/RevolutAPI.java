package xotv.android.network;

import retrofit2.Call;
import retrofit2.http.GET;
import xotv.android.network.Responses.CurrencyModel;

public interface RevolutAPI {
	@GET("/latest?base=EUR")
	Call<CurrencyModel> getCurrencies();
}
