package xotv.android.network.Responses;

import java.util.Map;

public class CurrencyModel {
	private Map<String,Double> rates;
	public Map<String, Double> getRates() {
		return rates;
	}
	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}
}
