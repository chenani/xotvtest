package xotv.android.Models;

public class CountryRateModel {

	private String countryCode;
	private Double rate;
	private boolean isSticked = false;

	public boolean isSticked() {
		return isSticked;
	}

	public void setSticked(boolean sticked) {
		isSticked = sticked;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
}
